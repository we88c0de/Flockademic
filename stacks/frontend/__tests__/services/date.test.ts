import { getFormattedDate, getFormattedDatetime } from '../../src/services/date';

it('should display a date with an unambiguous month name', () => {
  // Since the en-GB locale isn't available when running in Node,
  // we're expecting the en-US version here:
  expect(getFormattedDate('2002-02-14T13:37:00.000Z')).toBe('February 14, 2002');
});

it('should display a date and time with an unambiguous month name', () => {
  // Since the en-GB locale isn't available when running in Node,
  // we're expecting the en-US version here:
  expect(getFormattedDatetime('2002-02-14T13:37:00.000Z')).toBe('February 14, 2002, 1:37 PM');
});
