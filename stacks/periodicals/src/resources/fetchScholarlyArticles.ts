import { GetScholarlyArticlesResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { PublishedScholarlyArticle } from '../../../../lib/interfaces/ScholarlyArticle';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { fetchNewestIndependentScholarlyArticles, fetchScholarlyArticlesByAuthor } from '../services/scholarlyArticles';

export async function fetchScholarlyArticles(
  context: Request<undefined> & DbContext,
): Promise<GetScholarlyArticlesResponse> {
  if (!context.query || Object.keys(context.query).length === 0) {
    try {
      const articles: PublishedScholarlyArticle[] = await fetchNewestIndependentScholarlyArticles(context.database);

      return articles;
    } catch (e) {
      throw new Error('Could not load the latest articles, please try again.');
    }
  }

  if (context.query.author) {
    try {
      const articles = await fetchScholarlyArticlesByAuthor(context.database, context.query.author);

      return articles;
    } catch (e) {
        throw new Error(`Could not find articles for author ID \`${context.query.author}\`.`);
    }
  }

  throw(new Error('Fetching more than a single author\'s articles is not yet supported.'));
}
